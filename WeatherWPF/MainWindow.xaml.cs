﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using WeatherWPF.Models;

namespace WeatherWPF
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public Weather GetWeatherInformation(string cityName)
        {
            string url = $"https://api.apixu.com/v1/forecast.json?key=ba05380eeb844bb08c0123209190505&days=7&q={cityName}";
            string json;

            using (WebClient client = new WebClient())
            {
                json = client.DownloadString(url);
            }

            return JsonConvert.DeserializeObject<Weather>(json);
        }

        private void SearchButtonClick(object sender, RoutedEventArgs e)
        {
            const int ZERO = 0;
            const int FIRST = 1;
            const int SECOND = 2;
            const int THIRD = 3;
            const int FOURTH = 4;
            const int FIFTH = 5;
            const int SIXTH = 6;


            if (string.IsNullOrEmpty(cityNameTextBox.Text))
            {
                MessageBox.Show("Field empty!");
                return;
            }

            try
            {
                var weatherInformation = GetWeatherInformation(cityNameTextBox.Text);

                weatherImage.Source = new BitmapImage(new Uri("http:" + weatherInformation.Forecast.ForecastDay[ZERO].Day.Condition.Icon));

                temperature.Text = Convert.ToInt32(weatherInformation.Forecast.ForecastDay[ZERO].Day.MaximumTemperature).ToString()
                    + "° - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[ZERO].Day.MinimumTemperature).ToString() + "°";

                windSpeed.Text = "Wind speed - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[ZERO].Day.WindSpeed).ToString() + "Km/h";

                humidity.Text = "Humidity - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[ZERO].Day.AverageHumidity).ToString() + "%";

                precipitation.Text = "Precipitation - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[ZERO].Day.TotalPrecipitation).ToString() + "mm";

                visible.Text = "Visible - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[ZERO].Day.AverageVisible).ToString() + "Km";

                date.Text = weatherInformation.Forecast.ForecastDay[ZERO].Date.Value.DayOfWeek.ToString() + " - " + weatherInformation.Forecast.ForecastDay[ZERO].Date.Value.ToShortDateString();


                weatherImageFirst.Source = new BitmapImage(new Uri("http:" + weatherInformation.Forecast.ForecastDay[FIRST].Day.Condition.Icon));

                temperatureFirst.Text = Convert.ToInt32(weatherInformation.Forecast.ForecastDay[FIRST].Day.MaximumTemperature).ToString()
                    + "° - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[FIRST].Day.MinimumTemperature).ToString() + "°";

                windSpeedFirst.Text = "Wind speed - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[FIRST].Day.WindSpeed).ToString() + "Km/h";

                humidityFirst.Text = "Humidity - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[FIRST].Day.AverageHumidity).ToString() + "%";

                precipitationFirst.Text = "Precipitation - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[FIRST].Day.TotalPrecipitation).ToString() + "mm";

                visibleFirst.Text = "Visible - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[FIRST].Day.AverageVisible).ToString() + "Km";

                dateFirst.Text = weatherInformation.Forecast.ForecastDay[FIRST].Date.Value.DayOfWeek.ToString() + " - " + weatherInformation.Forecast.ForecastDay[FIRST].Date.Value.ToShortDateString();


                weatherImageSecond.Source = new BitmapImage(new Uri("http:" + weatherInformation.Forecast.ForecastDay[SECOND].Day.Condition.Icon));

                temperatureSecond.Text = Convert.ToInt32(weatherInformation.Forecast.ForecastDay[SECOND].Day.MaximumTemperature).ToString()
                    + "° - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[SECOND].Day.MinimumTemperature).ToString() + "°";

                windSpeedSecond.Text = "Wind speed - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[SECOND].Day.WindSpeed).ToString() + "Km/h";

                humiditySecond.Text = "Humidity - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[SECOND].Day.AverageHumidity).ToString() + "%";

                precipitationSecond.Text = "Precipitation - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[SECOND].Day.TotalPrecipitation).ToString() + "mm";

                visibleSecond.Text = "Visible - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[SECOND].Day.AverageVisible).ToString() + "Km";

                dateSecond.Text = weatherInformation.Forecast.ForecastDay[SECOND].Date.Value.DayOfWeek.ToString() + " - " + weatherInformation.Forecast.ForecastDay[SECOND].Date.Value.ToShortDateString();


                weatherImageThird.Source = new BitmapImage(new Uri("http:" + weatherInformation.Forecast.ForecastDay[THIRD].Day.Condition.Icon));

                temperatureThird.Text = Convert.ToInt32(weatherInformation.Forecast.ForecastDay[THIRD].Day.MaximumTemperature).ToString()
                    + "° - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[THIRD].Day.MinimumTemperature).ToString() + "°";

                windSpeedThird.Text = "Wind speed - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[THIRD].Day.WindSpeed).ToString() + "Km/h";

                humidityThird.Text = "Humidity - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[THIRD].Day.AverageHumidity).ToString() + "%";

                precipitationThird.Text = "Precipitation - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[THIRD].Day.TotalPrecipitation).ToString() + "mm";

                visibleThird.Text = "Visible - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[THIRD].Day.AverageVisible).ToString() + "Km";

                dateThird.Text = weatherInformation.Forecast.ForecastDay[THIRD].Date.Value.DayOfWeek.ToString() + " - " + weatherInformation.Forecast.ForecastDay[THIRD].Date.Value.ToShortDateString();


                weatherImageFourth.Source = new BitmapImage(new Uri("http:" + weatherInformation.Forecast.ForecastDay[FOURTH].Day.Condition.Icon));

                temperatureFourth.Text = Convert.ToInt32(weatherInformation.Forecast.ForecastDay[FOURTH].Day.MaximumTemperature).ToString()
                    + "° - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[FOURTH].Day.MinimumTemperature).ToString() + "°";

                windSpeedFourth.Text = "Wind speed - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[FOURTH].Day.WindSpeed).ToString() + "Km/h";

                humidityFourth.Text = "Humidity - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[FOURTH].Day.AverageHumidity).ToString() + "%";

                precipitationFourth.Text = "Precipitation - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[FOURTH].Day.TotalPrecipitation).ToString() + "mm";

                visibleFourth.Text = "Visible - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[FOURTH].Day.AverageVisible).ToString() + "Km";

                dateFourth.Text = weatherInformation.Forecast.ForecastDay[FOURTH].Date.Value.DayOfWeek.ToString() + " - " + weatherInformation.Forecast.ForecastDay[FOURTH].Date.Value.ToShortDateString();


                weatherImageFifth.Source = new BitmapImage(new Uri("http:" + weatherInformation.Forecast.ForecastDay[FIFTH].Day.Condition.Icon));

                temperatureFifth.Text = Convert.ToInt32(weatherInformation.Forecast.ForecastDay[FIFTH].Day.MaximumTemperature).ToString()
                    + "° - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[FIFTH].Day.MinimumTemperature).ToString() + "°";

                windSpeedFifth.Text = "Wind speed - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[FIFTH].Day.WindSpeed).ToString() + "Km/h";

                humidityFifth.Text = "Humidity - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[FIFTH].Day.AverageHumidity).ToString() + "%";

                precipitationFifth.Text = "Precipitation - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[FIFTH].Day.TotalPrecipitation).ToString() + "mm";

                visibleFifth.Text = "Visible - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[FIFTH].Day.AverageVisible).ToString() + "Km";

                dateFifth.Text = weatherInformation.Forecast.ForecastDay[FIFTH].Date.Value.DayOfWeek.ToString() + " - " + weatherInformation.Forecast.ForecastDay[FIFTH].Date.Value.ToShortDateString();


                weatherImageSixth.Source = new BitmapImage(new Uri("http:" + weatherInformation.Forecast.ForecastDay[SIXTH].Day.Condition.Icon));

                temperatureSixth.Text = Convert.ToInt32(weatherInformation.Forecast.ForecastDay[SIXTH].Day.MaximumTemperature).ToString()
                    + "° - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[SIXTH].Day.MinimumTemperature).ToString() + "°";

                windSpeedSixth.Text = "Wind speed - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[SIXTH].Day.WindSpeed).ToString() + "Km/h";

                humiditySixth.Text = "Humidity - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[SIXTH].Day.AverageHumidity).ToString() + "%";

                precipitationSixth.Text = "Precipitation - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[SIXTH].Day.TotalPrecipitation).ToString() + "mm";

                visibleSixth.Text = "Visible - " + Convert.ToInt32(weatherInformation.Forecast.ForecastDay[SIXTH].Day.AverageVisible).ToString() + "Km";

                dateSixth.Text = weatherInformation.Forecast.ForecastDay[SIXTH].Date.Value.DayOfWeek.ToString() + " - " + weatherInformation.Forecast.ForecastDay[SIXTH].Date.Value.ToShortDateString();

            }
            catch
            {
                MessageBox.Show("City not found!!!");
                return;
            }

            

        }
    }
}
